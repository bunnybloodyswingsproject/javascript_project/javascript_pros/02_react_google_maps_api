/* eslint-disable no-undef */
import './App.css';
import { 
  useJsApiLoader,
  GoogleMap,
  Marker,
  DirectionsRenderer
} from "@react-google-maps/api";
import { useEffect, useRef, useState } from 'react';

function App() {
  const {isLoaded} = useJsApiLoader({
    googleMapsApiKey: "AIzaSyBp33ZOyu7_UMpa-soeL2nbLHqVLckYVjw",
    libraries: ["places"]
  });

  const [center, setCenter] = useState({lat: 39.9042,lng: 116.4074 });
  const [direction, setDirection] = useState(null);
  const [waypoints, setWaypoints] = useState([
    {
      location: "South Dakota State University, Stadium Road, Brookings, SD, USA",
      stopover: true
    },
    {
      location: "Louisiana, MO, USA",
      stopover: true
    },
    {
      location: "Ohio, USA",
      stopover: true
    },
  ]);
  const [distance, setDistance] = useState("");
  const [duration, setDuration] = useState("");
  const [travelOptions, setTravelOptions] = useState("Car");
  const [startingPoint, setStartingPoint] = useState("Mexico City, CDMX, Mexico");
  const [endingPoint, setEndingPoint] = useState("Virginia Tech, Blacksburg, VA, USA");
  const [fromAtoB, setFromAtoB] = useState(null);
  const [fromBtoC, setFromBtoC] = useState(null);
  let mapRendererRef = useRef(false);
  const [viewpoints, setViewpoints] = useState(null);
  const mapRef = useRef(null);
  const [slider, setSlider] = useState(false);
  const google = window.google;
  useEffect(() => {
    const mapRenderer = async function calculateRoute() {
      if(startingPoint === "" || endingPoint === "") {
        alert("Please provide your location and desired destination");
      }
      const directionService = new google.maps.DirectionsService();
      const results = await directionService.route({
        origin: startingPoint,
        destination: endingPoint,
        waypoints: waypoints,
        // eslint-disable-next-line no-undef
        travelMode: travelOptions === "DRIVING" ? google.maps.TravelMode.DRIVING : travelOptions === "TRANSIT" ? google.maps.TravelMode.TRANSIT : google.maps.TravelMode.WALKING
      }).catch(err => {
        alert("Transportation method is not applicable");
      })

      setViewpoints(results.routes[0])
      setDirection(results);
      setDistance(results.routes[0].legs[0].distance.text);
      setDuration(results.routes[0].legs[0].duration.text);
  
      setFromAtoB(prev => {
        return {
          direction: {
            start: results.routes[0].legs[0].start_address,
            end: results.routes[0].legs[0].end_address
          },
          distance: results.routes[0].legs[0].distance.text,
          duration: results.routes[0].legs[0].duration.text
        }
      })
  
      setFromBtoC(prev => {
        return {
          direction: {
            start: results.routes[0].legs[1].start_address,
            end: results.routes[0].legs[1].end_address
          },
          distance: results.routes[0].legs[1].distance.text,
          duration: results.routes[0].legs[1].duration.text
        }
      })
    }

    mapRenderer();

    return () => mapRendererRef.current = true;
  }, [endingPoint, startingPoint, waypoints, travelOptions, google])

  if(!isLoaded) {
    return <p>Loading...</p>
  }

  const travel_method = [
    "DRIVING",
    "TRANSIT",
    "WALKING"
  ]

  const onChangeTravelMethod = (e) => {
    const value = e.target.value;
    setTravelOptions(value)
  }
  
  const closeSlider = () => {
    setSlider(prev => !prev)
  }

  return (
    <div className="App">
      <div className="map_container">
        <GoogleMap 
          center={center} 
          zoom={5} 
          mapContainerClassName="google_map_container"
          options={{
            zoomControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            fullscreenControl: false
          }}
          ref={mapRef}
        >
          <Marker center={center} />
          {
            direction && 
            <DirectionsRenderer 
              directions={direction}
            />
          }
        </GoogleMap>
      </div>
      <div className="google_directional_form">
        <div className="gmap_form_container">
          <div className="gmap_form_control">
            <input 
              type="text"
              value="Mexico City, CDMX, Mexico"
              disabled={true}
            />
            <div className="gmap_form_control_info">
              <small>Start To Waypoints: </small>
              <div className="gmap_form_control_waypoints_info">
                <div className="waypoints_group_info">
                  <small>{fromAtoB?.distance}</small>
                  {fromAtoB !== null && <small>/</small>}
                  <small>{fromAtoB?.duration}</small>
                </div>
              </div>
            </div>
          </div>
          <div className="gmap_form_control">
            <input 
              type="text"
              value="Virginia Tech, Blacksburg, VA, USA"
              disabled={true}
            />
            <div className="gmap_form_control_info">
              <small>Waypoint To Endpoint: </small>
              <div className="gmap_form_control_waypoints_info">
                <div className="waypoints_group_info">
                  <small>{fromBtoC?.distance}</small>
                  {fromAtoB !== null && <small>/</small>}
                  <small>{fromBtoC?.duration}</small>
                </div>
              </div>
            </div>
          </div>
          <div className="gmap_form_control">
            <input 
              type="text"
              value="South Dakota State University, Stadium Road, Brookings, SD, USA"
              disabled={true}
            />
          </div>
          <div className="gmap_form_control_icons">
            <div className="gmap_form_submit">
              <i className="fa-brands fa-telegram"></i>
            </div>
            <select onChange={onChangeTravelMethod}>
              {
                travel_method.map((method, i) => (
                  <option key={i} value={method}>{method}</option>
                ))
              }
            </select>
          </div>
        </div>
      </div>
      <div className={slider ? "gmap_table" : "gmap_table close"}>
        <table className="tftable" border="1">
          <tr>
            <th>From Location</th>
            <th>To Location</th>
            <th>Distance</th>
            <th>Duration</th>
          </tr>
          {
            viewpoints?.legs?.map((places, i) => (
              <tr key={i}>
                <td>{places?.start_address}</td>
                <td>{places?.end_address}</td>
                <td>{places?.distance?.text}</td>
                <td>{places?.duration?.text}</td>
              </tr>
            ))
          }
          <div className="table_close" onClick={closeSlider}>
          <i className="fa-solid fa-bars"></i>
          </div>
        </table>
      </div>
    </div>
  );
}

export default App;
